Given the following data, provide the answer to the following:
	a. List the books Authored by Marjorie Green.
		The Busy Executive's Database Guide
		You Can Combat Computer Stress!
	b. List the books Authored by Michael O'Leary.
		Cooking wih Computers
		TC7777 is not in the list
	c. Write the author/s of "The Busy Executives Database Guide".
		Marjorie Green
		Abraham Bennet
	d. Identify the publisher of "But Is It User Friendly?".
		Algodata Infosystems
	e. List the books published by Algodata Infosystems.
		The Busy Executive's Database Guide
		Cooking with Computers
		Straight Talk About Computers
		But Is It User Friendly?
		Secrets of Silicon Valley
		Net Etiquette
Create SQL Syntax and Queries to create a database based on the ERD:

Creating a Database:
	CREATE DATABASE blog_db;
Selecting a Database:
	USE blog_db;
Adding the Users Table:
	CREATE TABLE users (id INT NOT NULL AUTO_INCREMENT, email VARCHAR(100) NOT NULL, password VARCHAR(300) NOT NULL, datetime_created DATETIME NOT NULL, PRIMARY KEY(id));
Adding the Posts Table:
	CREATE TABLE posts (id INT NOT NULL AUTO_INCREMENT, author_id INT NOT NULL, title VARCHAR(500) NOT NULL, content VARCHAR(5000) NOT NULL, datetime_posted DATETIME NOT NULL, PRIMARY KEY(id), CONSTRAINT fk_posts_author_id FOREIGN KEY(author_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
Adding the Post_Comments Table:
	CREATE TABLE post_comments (id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL, user_id INT NOT NULL, content VARCHAR(5000) NOT NULL, datetime_commented DATETIME NOT NULL, PRIMARY KEY(id), CONSTRAINT fk_post_comments_post_id FOREIGN KEY(post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_post_comments_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
Adding the Post_Likes Table:
	CREATE TABLE post_likes (id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL, user_id INT NOT NULL, datetime_liked DATETIME NOT NULL, PRIMARY KEY(id), CONSTRAINT fk_post_likes_post_id FOREIGN KEY(post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_post_likes_user_id FOREIGN KEY(user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);

Finished!

